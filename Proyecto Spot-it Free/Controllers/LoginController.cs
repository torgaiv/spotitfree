﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Proyecto_Spot_it_Free.Controllers
{
    public class LoginController : Controller
    {
        private espotiEntities db = new espotiEntities();

        public string nick { get; set; }
        public string pass { get; set; }
        public object ErrorMessage { get; private set; }
        string error = "";

        // GET: Login
        public ActionResult Login()
            {
              return View();
            }

        [HttpPost]
        public ActionResult Login(Usuario user)
        {
            var obj = db.Usuario.FirstOrDefault(a => a.Nick == user.Nick && a.Pass == user.Pass);
            if (obj == null)
            {
                obj = db.Usuario.FirstOrDefault(a => a.Mail == user.Nick && a.Pass == user.Pass);
                if (obj == null)
                {
                    return RedirectToAction("Principal","Principal");
                }
            }
            Session["Usuario"] = obj;
            if (obj.Validado == 0)
            {
                return RedirectToAction("Index", "Validacion");
            }
                return RedirectToAction("UserDashBoard");            
        }

        public ActionResult UserDashBoard()
        {
            return View();
            /*      
              if (Session["Nick"] != null)
              {
                return View();
              }
              else
              {
                return RedirectToAction("Login");
              }*/
        }

        public ActionResult CerrarSesion()
        {
          return View();
        }

        public ActionResult Perfil()
        {
          return View();
        }
  }
}