﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Proyecto_Spot_it_Free.Models;

namespace Proyecto_Spot_it_Free.Controllers
{
    public class ValidacionController : Controller
    {
        public Utilidades u = new Utilidades();
        private espotiEntities db = new espotiEntities();

        // GET: Validacion
        public ActionResult Index()
        {
            Usuario user = (Usuario)Session["Usuario"];
            if (user.Validado == 0)
            {
                return View(u);
            }
            
            else {
                return RedirectToAction("Principal", "Principal");
            }
            
        }

        [HttpPost]
        public ActionResult Index(string apodoartista)
        {
            try
            {
                int codigo = Convert.ToInt32(apodoartista);
                int valida = devuelveCodigo();
                if (codigo == valida)
                {
                    ValidaUsuario();
                    Usuario user = (Usuario)Session["Usuario"];
                    string nick = user.Nick;
                    string pass = user.Pass;
                    Session.Clear();
                    var obj = db.Usuario.FirstOrDefault(a => a.Nick == nick && a.Pass == pass);
                    Session["Usuario"] = obj;
                }
                else
                {
                    Utilidades ue = new Utilidades("El codigo no coincide");
                    return View(ue);
                }
            }
            catch (Exception e) {
                Utilidades ue = new Utilidades("El codigo no coincide");
                return View(ue);
            }
            Usuario usser = (Usuario)Session["Usuario"];

            return RedirectToAction("Details", "Usuarios", new { id = usser.Nick });
        }

        public int devuelveCodigo() {
            int id = -1;
            Usuario user = (Usuario)Session["Usuario"];
            string nick = user.Nick;
            string query = "SELECT Codigo_validacion as codigo FROM USUARIO where nick='"+nick+"'";
            string cadena = System.Configuration.ConfigurationManager.ConnectionStrings["cadena1"].ConnectionString;
            SqlConnection conexion = new SqlConnection(cadena);
            conexion.Open();
            SqlCommand comando = new SqlCommand(query, conexion);
            SqlDataReader registros = comando.ExecuteReader();

            while (registros.Read())
            {
                try
                {
                    id = Convert.ToInt32(registros["codigo"].ToString());
                }
                catch (Exception e)
                {
                    id = 0;
                }
            }
            conexion.Close();
            return id;
        }

        public void ValidaUsuario( )
        {
            Usuario user = (Usuario)Session["Usuario"];
            string query = "update usuario set Validado=1 where nick='"+user.Nick+"'";
            string cadena = System.Configuration.ConfigurationManager.ConnectionStrings["cadena1"].ConnectionString;
            SqlConnection conexion = new SqlConnection(cadena);
            conexion.Open();
            SqlCommand comando = new SqlCommand(query, conexion);
            comando.ExecuteNonQuery();
            conexion.Close();
        }
    }
}