﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Proyecto_Spot_it_Free;

namespace Proyecto_Spot_it_Free.Controllers
{
    public class ArtistasController : Controller
    {
        private espotiEntities db = new espotiEntities();

        // GET: Artistas
        public async Task<ActionResult> Index()
        {
            
            var artista = db.Artista.Include(a => a.Usuario1);
            return View(await artista.ToListAsync());
        }

        // GET: Artistas/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Artista artista = await db.Artista.FindAsync(id);
            if (artista == null)
            {
                return HttpNotFound();
            }
            return View(artista);
        }

        // GET: Artistas/Create
        public ActionResult Create()
        {
            ViewBag.Usuario = new SelectList(db.Usuario, "Nick", "Nombre");
            return View();
        }

        // POST: Artistas/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Usuario,Nombre_artista")] Artista artista)
        {
            if (ModelState.IsValid)
            {
                db.Artista.Add(artista);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.Usuario = new SelectList(db.Usuario, "Nick", "Nombre", artista.Usuario);
            return View(artista);
        }

        // GET: Artistas/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Artista artista = await db.Artista.FindAsync(id);
            if (artista == null)
            {
                return HttpNotFound();
            }
            ViewBag.Usuario = new SelectList(db.Usuario, "Nick", "Nombre", artista.Usuario);
            return View(artista);
        }

        // POST: Artistas/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Usuario,Nombre_artista")] Artista artista)
        {
            if (ModelState.IsValid)
            {
                db.Entry(artista).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.Usuario = new SelectList(db.Usuario, "Nick", "Nombre", artista.Usuario);
            return View(artista);
        }

        // GET: Artistas/Delete/5
        public async Task<ActionResult> Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Artista artista = await db.Artista.FindAsync(id);
            if (artista == null)
            {
                return HttpNotFound();
            }
            return View(artista);
        }

        // POST: Artistas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            Artista artista = await db.Artista.FindAsync(id);

            List<Disco> c = new List<Disco>();
            c = artista.Disco.ToList<Disco>();

            for (int i = 0; i < c.Count; i++)
            {
                db.Disco.Remove(c[i]);
                await db.SaveChangesAsync();
            }

            db.Artista.Remove(artista);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
